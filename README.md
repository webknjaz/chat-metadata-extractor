# Metadata Extractor
This is a solution for extracting mentions, emoticons and rich links data from input messages.

CI: [![`master` branch status](https://codeship.com/projects/6ab41900-c2b0-0133-2c0f-46ca2537ba45/status?branch=master)](https://codeship.com/projects/137852)


## Requirements:

    Python 2.7.10 (it is recommended to use pyenv for dev environment)

## Usage:
```sh
$ pip install -r requirements/base.txt
```

```python
from metadata_extractor import ConfigReader, MetadataExtractor
config = 'metadata_extractor/extractor_config.yml'
# Construct dict-like config from YAML, may be replaced with normal dict
conf = ConfigReader(config)
# Build extractor with config
extractor = MetadataExtractor(conf)
# Extract as dict
dict_data = extractor.extract(input_message)
# Or extract as JSON string
json_data = extractor.extract_json(input_message)
```

## Testing:

```sh
$ pip install -r requirements/test.txt
$ nosetests --with-coverage
$ pre-commit run --all-files
```

## Development:
Let `pre-commit` install same-named hook into your local git repository to keep code quality acceptable seamlessly:

```
$ pre-commit install
```

## Notes

This package is shipped with default extraction rules config, witch is stored in YAML format in `metadata_extractor/extractor_config.yml`.
