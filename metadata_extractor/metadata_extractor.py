#! /usr/bin/env python
# -*- coding: utf-8 -*-

from collections import Mapping
import json

try:
    import re2 as re
except ImportError:
    import re

from twisted.internet.defer import inlineCallbacks, returnValue
from twisted.web.client import HTTPConnectionPool

from .utils import get_rich_media_list


class MetadataExtractor(object):
    """MetadataExtractor is a solution for extracting metadata from messages

    It may get mentions (@webknjaz), emoticons (wink) and URLs
    (and retrieve their page titles) from a post submitted for analysis.
    """
    _pool = None

    def __init__(self, config, pool=None):
        """Prepares extractor object for processing messages

        Args:
            config (dict): Configuration containing regexps matching slices
            pool (twisted.web.client.HTTPConnectionPool): Optional pool for
                                                          HTTP connections
        """
        assert isinstance(config, Mapping)
        self._config = config

        if pool is not None:
            assert isinstance(pool, HTTPConnectionPool)
            self._pool = pool

    def parse_message(self, message):
        """Extracts all metadata from a text and categorizes it

        Args:
            message (str): Input text for inspecting
        Returns:
            metadata (dict[str, list]): Mapping of metadata types and
                                        corresponding message slices
        """
        assert isinstance(message, basestring)

        # initialize empty result object
        metadata = {k: set() for k in self._config}

        # initialize slice criterion (needed to cut off extra symbols from
        # matched string)
        slices = {k: v.get('slice', {}) for k, v in self._config.iteritems()}

        # Optimize regexp to loop through all matches in a single iteration
        generic_regexp = '|'.join('(?P<{}>{})'.format(k, v['regexp'])
                                  for k, v in self._config.iteritems())

        # Extract metadata from message
        for excerpt in re.finditer(generic_regexp, message):
            for k in metadata:
                item = excerpt.group(k)
                if item is not None:
                    metadata[k].add(
                        item[slices[k].get('from'):slices[k].get('to')])
                    break

        # Filter out empty sets and convert what's left to lists
        return {k: list(v) for k, v in metadata.iteritems() if len(v)}

    @inlineCallbacks
    def extract(self, message):
        """Retrieves all emoticons, mentions and links w/ meta from message

        Args:
            message (str): Input text for inspecting
        Returns:
            metadata (dict): Mapping of metadata types and corresponding
                             message slices with their metadata if available
        """
        metadata = self.parse_message(message)
        _links = []

        for key in [k for k in metadata.keys()
                    if k not in ('mentions', 'emoticons')]:
            _links.extend(
                (yield get_rich_media_list(
                    metadata.pop(key), type_=key, pool=self._pool)))

        if _links:
            metadata['links'] = _links
        del _links

        returnValue(metadata)

    @inlineCallbacks
    def extract_json(self, message):
        """Retrieves all emoticons, mentions and links w/ meta from message

        Args:
            message (str): Input text for inspecting
        Returns:
            metadata (str): JSON string containing metadata types and
                            message slices with their metadata if fetched
        """
        res = yield self.extract(message)
        returnValue(json.dumps(res))

if __name__ == '__main__':
    from twisted.internet import reactor
    from .config_reader import ConfigReader

    test_message = '@webknjaz http://webknjaz.com (hiiii) @bluebirrrrd'
    config = 'metadata_extractor/extractor_config.yml'
    conf = ConfigReader(config)
    extractor = MetadataExtractor(conf)

    @inlineCallbacks
    def main():
        print('Extracting info from message: "{}"'.format(test_message))
        try:
            md = yield extractor.extract(test_message)

            print('Extracted metadata:')
            print(md)
        except Exception as e:
            from twisted.python import log
            log.err(e)

    reactor.callLater(0, main)
    reactor.callLater(5, reactor.stop)
    reactor.run()
