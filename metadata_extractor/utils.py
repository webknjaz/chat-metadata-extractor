import bs4  # aka BeautifulSoup

from twisted.internet.defer import inlineCallbacks, returnValue
from twisted.internet.error import (
    ConnectError, DNSLookupError,
    ConnectionLost, NotConnectingError, ConnectingCancelledError,
    UnsupportedAddressFamily, UnsupportedSocketType, InvalidAddressError,
)

from treq import get as treq_get


@inlineCallbacks
def retrieve_page_title(url, pool=None):
    """Fetches given URL's title from HTML <title> tag

    In case of inability to retrieve the title or network issues
    returns None.

    Args:
        url (str): Web page's URL
        pool (twisted.web.client.HTTPConnectionPool): Optional pool for
                                                      HTTP connections
    Returns:
        title (str) if the web page's title is found, None otherwise
    """
    try:
        resp = yield treq_get(url, pool=pool)
        page_contents = yield resp.text()
        soup = bs4.BeautifulSoup(page_contents, 'html.parser')
        returnValue(soup.title.text)
    # Swallow known errors, there's nothing we can do about missing title or
    # unreachable web-page. Survive anyway
    except (
        DNSLookupError,  # DNS lookup failed
        ConnectError,  # Base class for lots of connection errors
        ConnectionLost,  # Connection to the other side was uncleanly lost

        # Some internal network-related errors, I believe they should be
        # processed by Twisted internally. So trapping them 'just in case'
        NotConnectingError,  # Cannot stop connecting (invalid state)
        ConnectingCancelledError,  # Connection stopped before really connected
        UnsupportedAddressFamily,  # Address family unsupported by Twisted
        UnsupportedSocketType,  # Invalid socket type
        InvalidAddressError,  # Incorrect addr passed (e.g. IPv4 instead of v6)

        AttributeError,  # Appears when there's no <title> on a web-page
    ):
        returnValue(None)


@inlineCallbacks
def get_rich_media_list(links, type_='links', pool=None):
    """Generates iterable of given links metadatas

    Args:
        links (list[str]): List of links to retrieve titles for
        type_ (str): Given links list type, defaults to 'links'
        pool (twisted.web.client.HTTPConnectionPool): Optional pool for
                                                      HTTP connections
    Returns:
        link metadatas (list[dict]): Objects containing URL and
                                     corresponding page titles
    """
    prepend_proto = 'mailto:' if type_ == 'email' else ''

    returnValue([{
        'url': '{proto}{url}'.format(proto=prepend_proto, url=link),
        # fallback to link if title is not available for some reason
        'title': (link if not link.startswith('http')  # crawl only http(s)
                  else (yield retrieve_page_title(link, pool=pool)) or link)
    } for link in links])
