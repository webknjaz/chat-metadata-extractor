# -*- coding: utf-8 -*-

try:
    from collections.abc import Mapping
except ImportError:
    from collections import Mapping
finally:
    from collections import OrderedDict

import yaml


yaml.add_representer(
    OrderedDict,
    lambda dumper, data: dumper.represent_dict(data.iteritems()))
yaml.add_constructor(
    yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
    lambda loader, node: OrderedDict(loader.construct_pairs(node)))


class ConfigReader(Mapping):
    """ConfigReader keeps readonly instance of configuration dict"""

    def __init__(self, filepath):
        """Reads YAML config and stores it internally

        Args:
            filepath (int): The user ID to fetch.
        """
        with open(filepath, 'r') as config_file:
            self.__config = OrderedDict(yaml.load(config_file, yaml.Loader))

    def __getitem__(self, key):
        return self.__config[key]

    def __iter__(self):
        return iter(self.__config)

    def __len__(self):
        return len(self.__config)
