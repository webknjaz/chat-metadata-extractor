PINST=pip install --no-use-wheel -U
PINSTR=$(PINST) -r
REQ_DIR=requirements
READ=less
ISSUES_URL=https://bitbucket.org/webknjaz/chat-metadata-extractor/issues
OPEN_URL=xdg-open
PRECOMMIT=pre-commit
TRIAL=trial

all: readme

deps:
	@$(PINSTR) $(REQ_DIR)/base.txt

dev-deps: test-deps
	@$(PRECOMMIT) install

test-deps:
	@$(PINSTR) $(REQ_DIR)/test.txt

ci-deps:
	@$(PINSTR) $(REQ_DIR)/ci.txt

readme:
	@$(READ) README.md

issues:
	@echo "Please check out issues at $(ISSUES_URL)"

issue:
	@$(OPEN_URL) "$(ISSUES_URL)/$1"

help: readme issues
	@$(OPEN_URL) "$(ISSUES_URL)"

test: test-trial test-style
	@python --version

test-trial: test-deps
	@$(TRIAL) --coverage

test-style: test-deps
	@pre-commit run --all-files

test-ci: ci-deps
	@python --version
	@$(TRIAL) --coverage
	@pre-commit run --all-files
