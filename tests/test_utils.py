try:
    from unittest import mock
except ImportError:
    import mock

from twisted.internet.defer import inlineCallbacks
from twisted.trial.unittest import TestCase

from treq.testing import StringStubbingResource, StubTreq

from metadata_extractor.utils import get_rich_media_list, retrieve_page_title

from .utils import YAMLTestCasesMixin, YieldableTestCaseMixin


class TestUtils(YAMLTestCasesMixin, YieldableTestCaseMixin, TestCase):
    """Test suite for metadata_extractor.utils module"""
    yaml_cases = '../tests/test_cases/utils.yml'

    setUp = YAMLTestCasesMixin.setUp

    @inlineCallbacks
    def test_retrieve_page_title(self):
        for test_case in self.test_cases['retrieve_page_title']:
            resp = (200, {}, test_case.get('MockContent', b''))
            stub_treq = StubTreq(StringStubbingResource(lambda *args: resp))

            side_effect = test_case.get('SideEffect')
            if side_effect:
                def _side_effect(*args, **kwargs):
                    raise side_effect
                stub_treq.get = _side_effect

            with mock.patch(
                    'metadata_extractor.utils.treq_get', stub_treq.get):
                exc = test_case.get('Exception')
                if exc is not None:
                    if not issubclass(exc, Exception):
                        raise TypeError

                    with (yield self.assertYieldRaises(exc)):
                        yield retrieve_page_title(test_case['Input'])
                else:
                    ret = test_case['Return']
                    lnk_title = yield retrieve_page_title(test_case['Input'])
                    self.assertEquals(lnk_title, ret)

    @inlineCallbacks
    def test_get_rich_media_list(self):
        for test_case in self.test_cases['get_rich_media_list']:
            ret = test_case['Return']
            lnks = [test_case['Input']['link'], ]
            kwargs = {}
            type_ = test_case['Input'].get('type')
            if type_:
                kwargs['type_'] = type_

            get_title_mock = mock.MagicMock()
            get_title_mock.return_value = test_case['MockTitle']

            with mock.patch('metadata_extractor.utils.retrieve_page_title',
                            get_title_mock):
                lnk_info = (yield get_rich_media_list(lnks, **kwargs))[0]
                self.assertEquals(test_case['MockCalled'],
                                  get_title_mock.called)
                self.assertDictEqual(ret, lnk_info)
