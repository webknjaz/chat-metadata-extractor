# -*- coding: utf-8 -*-

import sys

try:
    from unittest import mock
except ImportError:
    import mock

from twisted.internet.defer import inlineCallbacks
from twisted.web.client import HTTPConnectionPool

from twisted.trial.test.test_util import StubReactor


from metadata_extractor import ConfigReader, MetadataExtractor

from .utils import HTTPTestCase, YAMLTestCasesMixin, YieldableTestCaseMixin


class TestMetadataExtractor(YAMLTestCasesMixin, YieldableTestCaseMixin,
                            HTTPTestCase):
    """Test MetadataExtractor information extraction abilities

    N.B. This is DSL implementation, testcases themselves are stored in YAML
    file, referred below.
    """
    yaml_cases = '../tests/test_cases/extractor.yml'

    # backwards compatibility hook:
    if sys.version_info < (3, 2, 0):
        assertCountEqual = HTTPTestCase.assertItemsEqual

    def setUp(self):
        # Maintains HTTP connections pool via self._http_pool
        super(TestMetadataExtractor, self).setUp()

        config = '../metadata_extractor/extractor_config.yml'
        conf = ConfigReader(config)
        self.extractor = MetadataExtractor(conf, pool=self._http_pool)

    def test_init(self):
        # incorrect config argument leads to an exception
        with self.assertRaises(AssertionError):
            MetadataExtractor(list())

        # # incorrect pool argument leads to an exception
        with self.assertRaises(AssertionError):
            MetadataExtractor(dict(), dict())

        # check that there's no exception thrown if correct args passes
        self.assertIsInstance(
            MetadataExtractor(
                # :seealso: https://github.com/twisted/twisted/blob/3b116ebd785f1ea0f9d8bf8fde27874b0f28a3df/src/twisted/trial/test/test_util.py#L331  # noqa
                dict(), HTTPConnectionPool(StubReactor([]), False)
            ),
            MetadataExtractor
        )

    def test_parse_message(self):
        # passing non-parsable input results in an exception
        with self.assertRaises(AssertionError):
            self.extractor.parse_message(None)

        # same
        with self.assertRaises(AssertionError):
            self.extractor.parse_message(dict())

        for test_case in self.test_cases:
            if 'Return' not in test_case:
                continue

            inp = test_case['Input']
            exp_res = {k: [r['title' if r['url'].startswith('mailto:')
                             else 'url']
                           if 'url' in r else r for r in v]
                       for k, v in test_case['Return'].iteritems()}
            md = self.extractor.parse_message(inp)
            md = {k if k in ('emoticons', 'mentions')
                  # convert all url type keys to 'links' key
                  else 'links': v
                  for k, v in md.iteritems()}

            for k in exp_res.iterkeys():
                self.assertCountEqual(exp_res[k], md[k])

    @inlineCallbacks
    def test_extract(self):
        """Tests whether extract() method

        Checks whether it calls correct functions internally
        and makes sure it pre-processes links
        """

        for test_case in self.test_cases:
            # skip tests for exception, since the method throws none of
            if 'Return' not in test_case:
                continue

            inp = test_case['Input']

            # calculate overall amount of links types expected
            exp_links_count = sum(1 for k in test_case['Return'].iterkeys()
                                  if k not in ('mentions', 'emoticons'))

            # generate expected result excluding links of any type
            exp_ret = {k: v for k, v in test_case['Return'].iteritems()
                       if k in ('mentions', 'emoticons')}

            # generate return value for parse_message() mock
            parse_mock_ret = {t: (v if t in ('mentions', 'emoticons')
                                  else [i['title' if t == 'email' else 'url']
                                        for i in v])
                              for t, v in test_case['Return'].iteritems()}

            _parse_message = mock.MagicMock(name='parse_message')
            _parse_message.return_value = parse_mock_ret

            _get_rich_media_list = mock.MagicMock(name='get_rich_media_list')
            _get_rich_media_list.return_value = (None, )

            with mock.patch('metadata_extractor.metadata_extractor.'
                            'MetadataExtractor.parse_message', _parse_message):
                with mock.patch('metadata_extractor.metadata_extractor.'
                                'get_rich_media_list', _get_rich_media_list):
                    ret = yield self.extractor.extract(inp)

            # pop links section, since we mock get_rich_media_list(),
            # which generates its contents
            ret.pop('links', [])

            # Assertions start:

            # make sure parse_message() has been called
            _parse_message.assert_called_with(inp)

            # expected result should match actual one, excluding links section
            self.assertEquals(exp_ret, ret)

            # parse_message() has to be called either way
            self.assertTrue(_parse_message.called)

            # get_rich_media_list() should be called for each link found
            self.assertEquals(exp_links_count, _get_rich_media_list.call_count)

    @inlineCallbacks
    def test_extract_json(self):
        """Tests internal calls flow of extract_json() method"""

        # mock for MetadataExtractor.extract()
        _extract_mock = mock.Mock(name='extract')
        _extract_mock.return_value = mock.sentinel.extract_ret_val

        # mock for json.dumps()
        _json_dumps_mock = mock.Mock(name='json_dumps')
        _json_dumps_mock.return_value = mock.sentinel.json_str

        with mock.patch('metadata_extractor.metadata_extractor.'
                        'MetadataExtractor.extract', _extract_mock):
            with mock.patch('metadata_extractor.metadata_extractor.'
                            'json.dumps', _json_dumps_mock):
                res = yield self.extractor.extract_json(mock.sentinel.inp_msg)

        # input argument has been bypassed into extract()
        _extract_mock.assert_called_with(mock.sentinel.inp_msg)

        # extract()'s return value was bypassed to json.dumps()
        _json_dumps_mock.assert_called_with(mock.sentinel.extract_ret_val)

        # and it has returned result w/o altering json.dumps()'s output
        self.assertIs(res, mock.sentinel.json_str)
