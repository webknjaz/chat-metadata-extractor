# -*- coding: utf-8 -*-

try:
    from unittest2 import TestCase
except ImportError:
    from unittest import TestCase

from metadata_extractor import ConfigReader


class TestConfigReader(TestCase):
    """Test ConfigReader yaml wrapper"""
    config = '../metadata_extractor/extractor_config.yml'

    def test_config_reader(self):
        conf = ConfigReader(self.config)
        self.assertEquals(conf['mentions']['regexp'], r'\@(?:\w+)')
        self.assertIsNone(conf['links'].get('slice'))
