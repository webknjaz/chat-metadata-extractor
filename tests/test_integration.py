# -*- coding: utf-8 -*-

import json

from twisted.internet.defer import inlineCallbacks


from metadata_extractor import ConfigReader, MetadataExtractor

from .utils import HTTPTestCase, YAMLTestCasesMixin, YieldableTestCaseMixin


class TestIntegration(YAMLTestCasesMixin, YieldableTestCaseMixin,
                      HTTPTestCase):
    """Test MetadataExtractor information extraction abilities

    N.B. This is DSL implementation, testcases themselves are stored in YAML
    file, referred below.
    """
    yaml_cases = '../tests/test_cases/extractor.yml'

    def setUp(self):
        # Maintains HTTP connections pool via self._http_pool
        super(TestIntegration, self).setUp()

        config = '../metadata_extractor/extractor_config.yml'
        conf = ConfigReader(config)
        self.extractor = MetadataExtractor(conf, pool=self._http_pool)

    @inlineCallbacks
    def test_extract(self):
        for test_case in self.test_cases:
            exc = test_case.get('Exception')
            if exc is not None:
                if not issubclass(exc, Exception):
                    raise TypeError

                with (yield self.assertYieldRaises(exc)):
                    yield self.extractor.extract(test_case['Input'])
            else:
                ret = test_case['Return']
                data = yield self.extractor.extract(test_case['Input'])
                for k in ret.iterkeys():
                    if isinstance(ret[k], dict):
                        self.assertDictEqual(data[k], ret[k])
                    else:
                        self.assertItemsEqual(data[k], ret[k])

    @inlineCallbacks
    def test_extract_json(self):
        for test_case in self.test_cases:
            inp = test_case['Input']
            exc = test_case.get('Exception')
            if exc is not None:
                if not issubclass(exc, Exception):
                    raise TypeError

                with (yield self.assertYieldRaises(exc)):
                    yield self.extractor.extract(inp)
            else:
                ret = test_case['Return']
                data = json.loads((yield self.extractor.extract_json(inp)))
                for k in ret.iterkeys():
                    if isinstance(ret[k], dict):
                        self.assertDictEqual(data[k], ret[k])
                    else:
                        self.assertItemsEqual(data[k], ret[k])
