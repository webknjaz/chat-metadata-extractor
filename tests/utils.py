# -*- coding: utf-8 -*-

try:
    from collections.abc import Sequence
except ImportError:
    from collections import Sequence
finally:
    from collections import OrderedDict

import yaml

from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks, returnValue
from twisted.internet.task import deferLater
from twisted.internet.tcp import Client
from twisted.trial.unittest import TestCase
from twisted.trial._synctest import _AssertRaisesContext
from twisted.web.client import HTTPConnectionPool


yaml.add_representer(
    OrderedDict,
    lambda dumper, data: dumper.represent_dict(data.iteritems()))
yaml.add_constructor(
    yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
    lambda loader, node: OrderedDict(loader.construct_pairs(node)))


class TestCasesReader(Sequence):
    """ConfigReader just keeps readonly list of cases"""

    def __init__(self, filepath):
        with open(filepath, 'r') as config_file:
            self.__tests = yaml.load(config_file, yaml.Loader)
            self.__len = len(self.__tests)

    def __getitem__(self, key):
        return self.__tests[key]

    def __len__(self):
        return self.__len

    def __del__(self):
        del self.__tests


class HTTPTestCase(TestCase):
    def setUp(self):
        super(HTTPTestCase, self).setUp()

        self._http_pool = HTTPConnectionPool(reactor, False)

    @inlineCallbacks
    def tearDown(self):
        """Cleans up test environment

        Shamelessly stolen from treq's tests
        :seealso: https://github.com/twisted/treq/issues/86#issuecomment-77701978  # noqa
        """

        super(HTTPTestCase, self).tearDown()

        @inlineCallbacks
        def _check_fds(_):
            # This appears to only be necessary for HTTPS tests.
            # For the normal HTTP tests then closeCachedConnections is
            # sufficient.
            fds = set(reactor.getReaders() + reactor.getReaders())
            if not [fd for fd in fds if isinstance(fd, Client)]:
                returnValue(None)

            yield deferLater(reactor, 0, _check_fds, None)

        try:
            yield self._http_pool.closeCachedConnections()
        finally:
            yield _check_fds(None)


class YAMLTestCasesMixin(object):
    """Test suite for metadata_extractor.utils module"""
    def setUp(self):
        super(YAMLTestCasesMixin, self).setUp()

        if not getattr(self, 'yaml_cases', None):
            raise EnvironmentError(
                'You should configure yaml_cases attribute first')

        self.test_cases = TestCasesReader(self.yaml_cases)


class YieldableTestCaseMixin(object):

    @inlineCallbacks
    def assertYieldRaises(self, exception, f=None, *args, **kwargs):
        """
        Fail the test unless calling the function C{f} with the given
        C{args} and C{kwargs} raises C{exception}. The failure will report
        the traceback and call stack of the unexpected exception.

        @param exception: exception type that is to be expected
        @param f: the function to call

        @return: If C{f} is C{None}, a context manager which will make an
            assertion about the exception raised from the suite it manages.  If
            C{f} is not C{None}, the exception raised by C{f}.

        @raise self.failureException: Raised if the function call does
            not raise an exception or if it raises an exception of a
            different type.
        """
        context = _AssertYieldRaisesContext(self, exception)
        if f is None:
            returnValue(context)

        @inlineCallbacks
        def _func():
            yield f(*args, **kwargs)

        returnValue((yield context._handle(_func)))


class _AssertYieldRaisesContext(_AssertRaisesContext):
    """
    A helper for implementing C{assertRaises}.  This is a context manager and a
    helper method to support the non-context manager version of
    C{assertRaises}.

    @ivar _testCase: See C{testCase} parameter of C{__init__}

    @ivar _expected: See C{expected} parameter of C{__init__}

    @ivar _returnValue: The value returned by the callable being tested (only
        when not being used as a context manager).

    @ivar _expectedName: A short string describing the expected exception
        (usually the name of the exception class).

    @ivar exception: The exception which was raised by the function being
        tested (if it raised one).
    """

    @inlineCallbacks
    def _handle(self, obj):
        """
        Call the given object using this object as a context manager.

        @param obj: The object to call and which is expected to raise some
            exception.
        @type obj: L{object}

        @return: Whatever exception is raised by C{obj()}.
        @rtype: L{BaseException}
        """
        with self as context:
            self._returnValue = yield obj()
        returnValue(context.exception)
